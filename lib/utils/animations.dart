import 'package:flutter/material.dart';

class Animations {
  static Widget fadeIn({required Widget child, required int delay}) {
    return TweenAnimationBuilder(
      child: child,
      tween: Tween(begin: 0.0, end: 1.0),
      duration: Duration(milliseconds: delay),
      builder: (context, value, child) {
        return Opacity(
          opacity: value as double,
          child: child,
        );
      },
    );
  }
}
