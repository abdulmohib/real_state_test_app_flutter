class Property {
  final String imageUrl;
  final String title;
  final String location;
  final double price;

  Property({
    required this.imageUrl,
    required this.title,
    required this.location,
    required this.price,
  });
}
