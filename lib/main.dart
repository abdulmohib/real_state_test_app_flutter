import 'package:flutter/material.dart';
import 'package:real_state_ui_test_app/widgets/customNavigationBar/custom_nav_bar_item.dart';

import 'screens/favorite_screen.dart';
import 'screens/home_screen.dart';
import 'screens/message_screen.dart';
import 'screens/profile_screen.dart';
import 'screens/search_screen.dart';
import 'widgets/customNavigationBar/custom_nav_bar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: MainScreen(),
    );
  }
}

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;

  static final List<Widget> _pages = <Widget>[
    SearchScreen(),
    const ChatScreen(),
    HomeScreen(),
    const FavoritesScreen(),
    const ProfileScreen(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: _pages[_selectedIndex],
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: CutomNavigationBar(
        dotIndicatorColor: Colors.orange,
        backgroundColor: Colors.black,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: [
          CustomNavigationBarItem(
            unselectedColor: Colors.black,
            icon: const Icon(Icons.search),
            selectedColor: Colors.white,
          ),
          CustomNavigationBarItem(
            unselectedColor: Colors.black,
            icon: const Icon(Icons.chat_bubble),
            selectedColor: Colors.white,
          ),
          CustomNavigationBarItem(
            unselectedColor: Colors.black,
            icon: const Icon(Icons.home),
            selectedColor: Colors.white,
          ),
          CustomNavigationBarItem(
            unselectedColor: Colors.black,
            icon: const Icon(Icons.favorite),
            selectedColor: Colors.white,
          ),
          CustomNavigationBarItem(
            unselectedColor: Colors.black,
            icon: const Icon(Icons.person),
            selectedColor: Colors.white,
          ),
        ],
      ),
    );
  }
}

class CustomBottomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      notchMargin: 8.0,
      color: Colors.black,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
                icon: const Icon(Icons.search, color: Colors.white),
                onPressed: () {}),
            IconButton(
                icon: const Icon(Icons.chat_bubble, color: Colors.white),
                onPressed: () {}),
            const SizedBox(width: 48), // space for the floating action button
            IconButton(
                icon: const Icon(Icons.favorite, color: Colors.white),
                onPressed: () {}),
            IconButton(
                icon: const Icon(Icons.person, color: Colors.white),
                onPressed: () {}),
          ],
        ),
      ),
    );
  }
}

class OfferCard extends StatefulWidget {
  final String title;
  final String offers;
  final int index;

  const OfferCard(
      {required this.title, required this.offers, required this.index});

  @override
  State<OfferCard> createState() => _OfferCardState();
}

class _OfferCardState extends State<OfferCard>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );

    _animation = CurvedAnimation(
      parent: _controller,
      curve: Curves.easeOutBack,
    );

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScaleTransition(
      scale: _animation,
      child: Container(
        width: MediaQuery.of(context).size.width / 2 - 30,
        height: MediaQuery.of(context).size.width / 2 - 30,
        padding: const EdgeInsets.all(16.0),
        decoration: widget.index == 1
            ? BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
                boxShadow: const [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10,
                    offset: Offset(0, 5),
                  ),
                ],
              )
            : const BoxDecoration(
                color: Colors.orange,
                shape: BoxShape.circle,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10,
                    offset: Offset(0, 5),
                  ),
                ],
              ),
        child: Column(
          children: [
            Text(
              widget.title,
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: widget.index == 0 ? Colors.white : Colors.grey),
            ),
            const SizedBox(height: 30),
            AnimatedFlipCounter(
                value: int.parse(
                  widget.offers,
                ),
                thousandSeparator: ' ',
                textStyle: TextStyle(
                    fontSize: 40,
                    fontWeight: FontWeight.bold,
                    color: widget.index == 0 ? Colors.white : Colors.grey),
                duration: const Duration(seconds: 1)),
            // Text(
            //   offers,
            //   style: TextStyle(
            //       fontSize: 40,
            //       fontWeight: FontWeight.bold,
            //       color: index == 0 ? Colors.white : Colors.grey),
            // ),
            Text(
              'offers',
              style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  color: widget.index == 0 ? Colors.white : Colors.grey),
            ),
          ],
        ),
      ),
    );
  }
}

class AnimatedFlipCounter extends StatefulWidget {
  final int value;
  final Duration duration;
  final TextStyle textStyle;
  final String thousandSeparator;

  const AnimatedFlipCounter({
    required this.value,
    required this.duration,
    required this.textStyle,
    this.thousandSeparator = ',',
  });

  @override
  _AnimatedFlipCounterState createState() => _AnimatedFlipCounterState();
}

class _AnimatedFlipCounterState extends State<AnimatedFlipCounter>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  int _currentValue = 0;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: widget.duration,
    );

    _animation = Tween<double>(
      begin: 0,
      end: widget.value.toDouble(),
    ).animate(_controller)
      ..addListener(() {
        setState(() {
          _currentValue = _animation.value.toInt();
        });
      });

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      _currentValue.toString().replaceAllMapped(
            RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
            (Match match) => '${match[1]}${widget.thousandSeparator}',
          ),
      style: widget.textStyle,
    );
  }
}
