import 'package:flutter/material.dart';

class AnimatedTextContainer extends StatefulWidget {
  const AnimatedTextContainer({super.key});

  @override
  _AnimatedTextContainerState createState() => _AnimatedTextContainerState();
}

class _AnimatedTextContainerState extends State<AnimatedTextContainer>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1), // Animation duration
    );

    animation = Tween<double>(begin: 0, end: 1).animate(
      CurvedAnimation(
        parent: controller,
        curve: Curves.easeInOut, // Animation curve
      ),
    );

    // Start the animation
    controller.forward();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Animated container width controlled by animation value
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget? child) {
        return Flexible(
          child: Container(
            height: 40,
            width: MediaQuery.of(context).size.width / 2.3 * animation.value,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(1), // Faded white background
              borderRadius: BorderRadius.circular(8),
            ),
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                const SizedBox(width: 8),
                const Icon(
                  Icons.location_on,
                  color: Colors.grey,
                  size: 16,
                ),
                const SizedBox(width: 4),
                Opacity(
                  opacity: animation
                      .value, // Text opacity controlled by animation value
                  child: const Text(
                    'Saint Petersburg',
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.grey,
                      // fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(width: 8),
              ],
            ),
          ),
        );
      },
    );
  }
}
