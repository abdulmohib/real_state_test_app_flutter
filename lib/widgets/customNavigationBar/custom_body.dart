import 'package:flutter/material.dart';
import 'package:real_state_ui_test_app/widgets/customNavigationBar/custom_nav_bar_item.dart';

class CustomBody extends StatelessWidget {
  const CustomBody({
    super.key,
    required this.items,
    required this.currentIndex,
    required this.curve,
    required this.duration,
    required this.selectedItemColor,
    required this.theme,
    required this.unselectedItemColor,
    required this.onTap,
    required this.itemPadding,
    required this.dotIndicatorColor,
    required this.enablePaddingAnimation,
    this.splashBorderRadius,
    this.splashColor,
  });

  final List<CustomNavigationBarItem> items;
  final int currentIndex;
  final Curve curve;
  final Duration duration;
  final Color? selectedItemColor;
  final ThemeData theme;
  final Color? unselectedItemColor;
  final Function(int index) onTap;
  final EdgeInsets itemPadding;
  final Color? dotIndicatorColor;
  final bool enablePaddingAnimation;
  final Color? splashColor;
  final double? splashBorderRadius;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        for (final item in items)
          TweenAnimationBuilder<double>(
            tween: Tween(
              end: items.indexOf(item) == currentIndex ? 1.0 : 0.0,
            ),
            curve: curve,
            duration: duration,
            builder: (context, t, _) {
              final selectedColor =
                  item.selectedColor ?? selectedItemColor ?? theme.primaryColor;

              final unselectedColor = item.unselectedColor ??
                  unselectedItemColor ??
                  theme.iconTheme.color;

              return CircleAvatar(
                  radius: items.indexOf(item) == currentIndex ? 25 : 18,
                  backgroundColor:
                      Color.lerp(Colors.grey[900], Colors.orange, t),
                  child: InkWell(
                    onTap: () => onTap.call(items.indexOf(item)),
                    focusColor: splashColor ?? selectedColor.withOpacity(0.1),
                    highlightColor:
                        splashColor ?? selectedColor.withOpacity(0.1),
                    splashColor: splashColor ?? selectedColor.withOpacity(0.1),
                    hoverColor: splashColor ?? selectedColor.withOpacity(0.1),
                    child: IconTheme(
                      data: IconThemeData(
                        color: Color.lerp(unselectedColor, selectedColor, t),
                        size: items.indexOf(item) == currentIndex ? 24 : 18,
                      ),
                      child: item.icon,
                    ),

                    // ClipRect(
                    //   child: SizedBox(
                    //     height: 40,
                    //     child: Align(
                    //       alignment: Alignment.bottomCenter,
                    //       widthFactor: t,
                    //       child: Padding(
                    //         padding: EdgeInsetsDirectional.only(
                    //             start: itemPadding.right / 0.63,
                    //             end: itemPadding.right),
                    //         child: DefaultTextStyle(
                    //           style: TextStyle(
                    //             color: Color.lerp(
                    //                 selectedColor.withOpacity(0.0),
                    //                 selectedColor,
                    //                 t),
                    //             fontWeight: FontWeight.w600,
                    //           ),
                    //           child: CircleAvatar(
                    //               radius: 2.5,
                    //               backgroundColor:
                    //                   dotIndicatorColor ?? selectedColor),
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ));
            },
          ),
      ],
    );
  }
}
