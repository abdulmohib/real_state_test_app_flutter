import 'package:flutter/material.dart';

class PropertyCard extends StatefulWidget {
  final String address;
  final String imageUrl;

  const PropertyCard(
      {super.key, required this.address, required this.imageUrl});

  @override
  State<PropertyCard> createState() => _PropertyCardState();
}

class _PropertyCardState extends State<PropertyCard>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );

    _animation = Tween<double>(
      begin: -1.0,
      end: 0.0,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    ));

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.blueGrey,
          borderRadius: BorderRadius.circular(12),
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 10,
              offset: Offset(0, 5),
            ),
          ],
        ),
        child: Stack(
          children: [
            // Flexible(
            //   child: ClipRRect(
            //     borderRadius:
            //         const BorderRadius.vertical(top: Radius.circular(12)),
            //     child: Image.network(widget.imageUrl,
            //         width: double.infinity, fit: BoxFit.cover),
            //   ),
            // ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Positioned(
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: AnimatedBuilder(
                      animation: _animation,
                      builder: (context, child) {
                        return Transform.translate(
                          offset: Offset(
                              MediaQuery.of(context).size.width *
                                  _animation.value,
                              0),
                          child: Container(
                            padding: const EdgeInsets.all(
                                8.0), // Optional: Add padding around the Row
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(
                                  0.3), // Optional: Set the background color of the Container
                              borderRadius: BorderRadius.circular(
                                  50.0), // Optional: Add rounded corners to the Container
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.white.withOpacity(0),
                                  spreadRadius: 2,
                                  blurRadius: 5,
                                  offset: const Offset(
                                      0, 3), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text(
                                    widget.address,
                                    style: const TextStyle(
                                      fontSize: 16,
                                      color: Colors.black38,
                                    ),
                                  ),
                                ),
                                AnimatedBuilder(
                                  animation: _animation,
                                  builder: (context, child) {
                                    return Transform.translate(
                                      offset: Offset(
                                          MediaQuery.of(context).size.width /
                                              5 *
                                              _animation.value,
                                          0),
                                      child: child,
                                    );
                                  },
                                  child: const Padding(
                                    padding: EdgeInsets.only(right: 5.0),
                                    child: CircleAvatar(
                                      radius: 20,
                                      backgroundColor: Colors.white,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        size: 15,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
