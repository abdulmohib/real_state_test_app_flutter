import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import 'property_card.dart';

class AnimatedSliverToBoxAdapter extends StatefulWidget {
  @override
  _AnimatedSliverToBoxAdapterState createState() =>
      _AnimatedSliverToBoxAdapterState();
}

class _AnimatedSliverToBoxAdapterState extends State<AnimatedSliverToBoxAdapter>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _opacityAnimation;
  late Animation<Offset> _positionAnimation;

  List<String> images = [
    'https://i.pinimg.com/474x/77/52/20/775220e5cd375c2391de0c12df69cd72.jpg',
    'https://st.hzcdn.com/simgs/pictures/exteriors/bayside-residence-by-design-img~dee1490601ffd6ef_14-2186-1-647f854.jpg',
    'https://media.istockphoto.com/id/856794670/photo/beautiful-luxury-home-exterior-with-green-grass-and-landscaped-yard.jpg?s=612x612&w=0&k=20&c=Jaun3vYekdy6aBcqq5uDQp_neNp5jmdLZXZAqqhcjk8=',
    'https://images.pexels.com/photos/1396132/pexels-photo-1396132.jpeg?cs=srgb&dl=pexels-binyaminmellish-1396132.jpg&fm=jpg',
    'https://images.pexels.com/photos/1396132/pexels-photo-1396132.jpeg?cs=srgb&dl=pexels-binyaminmellish-1396132.jpg&fm=jpg',
    'https://i.pinimg.com/474x/77/52/20/775220e5cd375c2391de0c12df69cd72.jpg'
  ];

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );

    _opacityAnimation = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    ));

    _positionAnimation = Tween<Offset>(
      begin: const Offset(0, 0.3),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.easeInOut,
    ));

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return FadeTransition(
          opacity: _opacityAnimation,
          child: SlideTransition(
            position: _positionAnimation,
            child: child,
          ),
        );
      },
      child: SliverToBoxAdapter(
        child: Container(
          color: Colors.white,
          child: StaggeredGrid.count(
            crossAxisCount: 4,
            mainAxisSpacing: 4,
            crossAxisSpacing: 4,
            children: [
              StaggeredGridTile.count(
                crossAxisCellCount: 4,
                mainAxisCellCount: 2,
                child: PropertyCard(
                  address: 'Barcelona, street 1',
                  imageUrl: images[0],
                ),
              ),
              StaggeredGridTile.count(
                crossAxisCellCount: 2,
                mainAxisCellCount: 4,
                child: PropertyCard(
                  address: 'valencia',
                  imageUrl: images[1],
                ),
              ),
              StaggeredGridTile.count(
                crossAxisCellCount: 2,
                mainAxisCellCount: 2,
                child: PropertyCard(
                  address: 'Gipuzkoa',
                  imageUrl: images[2],
                ),
              ),
              StaggeredGridTile.count(
                crossAxisCellCount: 2,
                mainAxisCellCount: 2,
                child: PropertyCard(
                  address: 'Seville',
                  imageUrl: images[3],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
