import 'package:flutter/material.dart';

class AnimatedCircleAvatar extends StatefulWidget {
  final String imageUrl;

  const AnimatedCircleAvatar({required this.imageUrl});

  @override
  _AnimatedCircleAvatarState createState() => _AnimatedCircleAvatarState();
}

class _AnimatedCircleAvatarState extends State<AnimatedCircleAvatar>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1), // Animation duration
    );

    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: _controller, curve: Curves.easeInOutCirc),
    );

    // Start the animation
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation,
      builder: (context, child) {
        return Transform.scale(
          scale: _animation.value,
          child: CircleAvatar(
            radius: 22,
            backgroundImage: NetworkImage(widget.imageUrl),
          ),
        );
      },
    );
  }
}
